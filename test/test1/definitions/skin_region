class skin_region
# The Skin Middleware
version version 0.8.0.486
author Shahbaz Youssefi
keyword skin
keyword middleware
keyword DIST
keyword MacLAB
shortcut index
shortcut globals
shortcut constants
previous class skin_sensor_type
next class skin_sub_region
seealso `[skin_sensor]`
seealso `[skin_sub_region]`
seealso `[skin_sensor_iterator]`
seealso `[#skinO_regions](skin_object)`
seealso `[#skinO_sub_region_indices](skin_object)`

The `skin_region` class holds data of one region of the skin.

VARIABLE skinR_id: skin_region_id
	Id of the region

	The id of the region. This value is equal to the region's index in the region list obtainable via `[#skinO_regions](skin_object)` function.
	If the value of `skinR_id` is equal to `[#SKIN_INVALID_ID](constants)`, that means the region is not properly initialized.
	See also `[#skin_region_id](skin_object)`.

VARIABLE skinR_sub_region_indices_begin: skin_sub_region_index_id
	Where the indices to the sub-regions of this region start

	The indices to sub-regions of each region are stored consequently. They are located in the sub-region-indices list obtainable via
	`[#skinO_sub_region_indices](skin_object)` function, with indices in \[`skinR_sub_region_indices_begin`, `[#skinR_sub_region_indices_end]`).

	**Note:** Unlike modules and patches, this index is not to the sub-regions array itself, but to a different
	array, called _sub_region-indices_ which has values indexing the sub-regions array. The reason for this is that the
	regions could share sub-regions and thus the sub-regions cannot be arranged in a way that would have a simple range such as
	\[a, b) defining every region. Note also that this implies that although within a region's \[`skinR_sub_region_indices_begin`,
	`skinR_sub_region_indices_end`) range the sub-region indices are unique, there may be duplicate sub-region indices in the
	whole sub-region-indices array. See also `[#skin_sub_region_index_id](skin_object)`.

VARIABLE skinR_sub_region_indices_end: skin_sub_region_index_id
	Where the indices to the sub-regions of this region end

	The indices to sub-regions of each region are stored consequently. They are located in the sub-region-indices list obtainable via
	`[#skinO_sub_region_indices](skin_object)` function, with indices in \[`[#skinR_sub_region_indices_begin]`, `skinR_sub_region_indices_end`).

	**Note:** Unlike modules and patches, this index is not to the sub-regions array itself, but to a different
	array, called _sub_region-indices_ which has values indexing the sub-regions array. The reason for this is that the
	regions could share sub-regions and thus the sub-regions cannot be arranged in a way that would have a simple range such as
	\[a, b) defining every region. Note also that this implies that although within a region's \[`skinR_sub_region_indices_begin`,
	`skinR_sub_region_indices_end`) range the sub-region indices are unique, there may be duplicate sub-region indices in the
	whole sub-region-indices array. See also `[#skin_sub_region_index_id](skin_object)`.

VARIABLE skinR_active: bool
	Indicates whethere there is an active sensor in this region or not

	This is a single boolean value indicating whether there is activity in this region. If there is, then the region's sub-regions
	can be probed and through the [sub-regions-activation array](skin_sub_region#skinSR_activations), the active sensors can be pinned down to a
	certain layer in a certain sub-region. The sensors in this sub-region can then be further inspected if more precise information are needed.

FUNCTION skin_region: ()
	Class constructor

	Default constructor of the class. Sets the region variables so that the region would be invalid.

FUNCTION ~skin_region: ()
	Class destructor

	Destructor of the class.

FUNCTION skinR_sub_region_indices: (count: skin_sub_region_index_size * = NULL): skin_sub_region_index_id *
	Gives an array of indices to sub-regions belonging to this region

	This function returns a reference to a part of the [sub-region-indices array](skin_object#skinO_sub_region_indices) where indices to the
	sub-regions of this region are located. That is, sub-regions of this region are the ones indexed in the sub-region-indices array from index
	`[#skinR_sub_region_indices_begin]`. The array will have **`count`** elements. Note that this is **not** a copy of the sub-region-indices array,
	but a reference to the original. See also `[#skin_sub_region_index_size](skin_object)` and `[#skin_sub_region_index_id](skin_object)`.

	In case all sensors of the region are intended to be traversed, it is advised to use an [iterator](#skinR_sensors_begin).

	INPUT count
		After `skinRSubRegions` finishes, this variable will hold the number of sub-regions in this region. If `NULL` is passed, or the
		function is called with no inputs, this input will be ignored.
	OUTPUT
		As output, this function returns an array of indices to sub-regions belonging to this region. If the region is not properly
		intialized, this function will return `NULL` and the input **`count`** will be left untouched.

FUNCTION skinR_sensors_begin: (): skin_region::skin_sensor_iterator
	Gives an iterator to iterate over sensors of this region

	This function returns a pointer of type `skin_region::[skin_sensor_iterator]`, similar to C++'s STL `iterator` classes, which points to the
	first of this region's sensors. This iterator can then be used to traverse all sensors of this region. Note that the sensors traversed are
	not a copy and the iterators actually point to the original data.

	The sensors are guaranteed to be traversed layer by layer, in order of layer id.

	OUTPUT
		The output of this function is an iterator over sensors of this region, pointing to the first one.

FUNCTION skinR_sensors_end: (): const skin_region::skin_sensor_iterator
	Gives an invalid iterator marking the end of iteration over the sensors

	This function returns a pointer of type `skin_region::[skin_sensor_iterator]`, similar to C++'s STL `iterator` classes, which points to an invalid
	sensor. This iterator can then be used in comparison with another iterator to determine whether that iterator has reached the end of its range.
	This is similar to how iterators are used in C++'s STL.

	OUTPUT
		The output of this function is an iterator over sensors of this region pointing to an invalid sensor (and therefore should not be dereferenced).

FUNCTION skinR_sub_regions_begin: (): skin_region::skin_sub_region_iterator
	Gives an iterator to iterate over sub-regions of this region

	This function returns a pointer of type `skin_region::[skin_sub_region_iterator]`, similar to C++'s STL `iterator` classes, which points to the first
	of this region's sub-regions. This iterator can then be used to traverse all sub-regions of this region. Note that the sub-regions traversed are not
	a copy and the iterators actually point to the original data.

	OUTPUT
		The output of this function is an iterator over sub-regions of this region, pointing to the first one.

FUNCTION skinR_sub_regions_end: (): const skin_region::skin_sub_region_iterator
	Gives an invalid iterator marking the end of iteration over the sub-regions

	This function returns a pointer of type `skin_region::[skin_sub_region_iterator]`, similar to C++'s STL `iterator` classes, which points to an invalid
	sub-region. This iterator can then be used in comparison with another iterator to determine whether that iterator has reached the end of its range.
	This is similar to how iterators are used in C++'s STL.

	OUTPUT
		The output of this function is an iterator over sub-regions of this region pointing to an invalid sub-region (and therefore should not be dereferenced).
