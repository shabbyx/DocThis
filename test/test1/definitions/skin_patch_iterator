class skin_patch_iterator
# The Skin Middleware
version version 0.8.0.486
author Shahbaz Youssefi
keyword skin
keyword middleware
keyword DIST
keyword MacLAB
shortcut index
shortcut globals
shortcut constants
previous class skin_object
next class skin_patch
seealso `[skin_object]`

The `skin_patch_iterator` is a class inside class `[skin_object]` which can be used to iterate through its patches in a C++ STL fashion, hiding the
complexities of the underlying structure. The usage of this class is similar to C++'s STL:
	```
	skin_object skin;
	for (skin_object::skin_patch_iterator i = skin.skinO_patches_begin(); i != skin.skinO_patches_end(); ++i)
	    cout \<\< i-\>skinP_id \<\< endl;
	```

FUNCTION operator ++: (): skin_patch_iterator \&
	Prefix increment of the iterator

	Moves the iterator ahead, which means it would now point to the next patch. Note that this operation does not perform range checks, i.e. with this
	operation, it is possible to go out of array.

	OUTPUT
		Returns a reference to the same `skin_patch_iterator` object which has its pointer incremented.

FUNCTION operator ++: (unused: int): skin_patch_iterator
	Postfix increment of the iterator

	Moves the iterator ahead, which means it would now point to the next patch. Note that this operation does not perform
	range checks, i.e. with this operation, it is possible to go out of array. Note also that the postfix function takes a
	copy of the object, increment the original one and return the copy, which might be unnecessary and the
	[prefix increment](#operator ++) is advised if it is to be used in an standalone statement, such as in a `for` loop.

	INPUT unused
		For C++ to differentiate between the postfix and prefix increments, the postfix increment has an `int` in its input list which is disregarded.
		Therefore, if called like `i.operator++(0)`, then this function would be called instead of the [prefix increment](#operator ++).
	OUTPUT
		Returns a copy of the same `skin_patch_iterator` object before having incremented it.

FUNCTION operator ==: (rhs: const skin_patch_iterator \&): bool
	Tests whether two iterators are equal

	Checks whether this iterator and **`rhs`** point to the same patch. Note that if both iterators are invalid, the result of this operation is
	**not necessarily** `true`.

	**Note:** `operator ++` can still be used on an invalid iterator, which means when the iterator reaches its end, it would be equal to what
	`[skinO_patches_end()](skin_object#skinO_patches_end)` returns, but if incremented more, they wouldn't be equal. Therefore, it is important that as soon
	as the iterator gets invalid, it is identified as invalid (checked against `skinO_patches_end`) and not incremented any more.

	INPUT rhs
		The right hand side of the equality.
	OUTPUT
		The result of the operation is whether the two iterators point to the same patch or not.

FUNCTION operator !=: (rhs: const skin_patch_iterator \&): bool
	Tests whether two iterators are unequal

	Checks whether this iterator and **`rhs`** point to different patches. Note that if both iterators are invalid, the result of this operation is
	**not necessarily** `false`.

	**Note:** `operator ++` can still be used on an invalid iterator, which means when the iterator reaches its end, it would be equal to what
	`[skinO_patches_end()](skin_object#skinO_patches_end)` returns, but if incremented more, they wouldn't be equal. Therefore, it is important that as soon
	as the iterator gets invalid, it is identified as invalid and not incremented any more.

	INPUT rhs
		The right hand side of the inequality.
	OUTPUT
		The result of the operation is whether the two iterators point to different patches or not.

FUNCTION operator *: (): skin_patch \&
	Get contents of the iterator

	Returns a reference to the original patch this iterator is pointing to so it could be used. Unless for copying, the programmer would be more
	interested in `[#operator -\>]`.

	OUTPUT
		Returns a reference to the patch this iterator is pointing to.

FUNCTION operator -\>: (): skin_patch *
	Get members of the object the iterator is pointing to

	Returns a pointer to the original patch this iterator is pointing to and then C++ uses that pointer to allow access to the `[skin_patch]`'s
	public variables and functions. Thus, as illustrated on the top of this page, working with the iterator is similar to working with pointers.

	OUTPUT
		Returns a pointer to the patch this iterator is pointing to.

FUNCTION operator skin_patch *: (): skin_patch *
	Cast the iterator to the patch object itself

	Casts the iterator to a pointer to the corresponding `[skin_patch]` object. This means that the iterator can be passed to functions taking
	`skin_patch *` as argument without a need for explicit cast.

	OUTPUT
		A pointer to the patch object this iterator is pointing to.
