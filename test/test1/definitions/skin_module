class skin_module
# The Skin Middleware
version version 0.8.0.486
author Shahbaz Youssefi
keyword skin
keyword middleware
keyword DIST
keyword MacLAB
shortcut index
shortcut globals
shortcut constants
previous class skin_sensor
next class skin_patch
seealso `[skin_sensor]`
seealso `[skin_patch]`
seealso `[skin_sensor_iterator]`
seealso `[#skinO_modules](skin_object)`

The `skin_module` class holds data of one module in the skin.

VARIABLE skinM_id: skin_module_id
	Id of the module

	The id of the module. This value is equal to the module's index in the module list obtainable via `[#skinO_modules](skin_object)`
	function. If the value of `skinM_id` is equal to `[#SKIN_INVALID_ID](constants)`, that means the module is not properly initialized.
	See also `[#skin_module_id](skin_object)`.

VARIABLE skinM_patch_id: skin_patch_id
	Id of the patch this module belongs to

	The id of the patch the module belongs to. See `[#skinP_id](skin_patch)` and `[#skin_patch_id](skin_object)`.

VARIABLE skinM_sensor_ids_begin: skin_sensor_id
	Where the sensors belonging to this module start

	The sensors of each module are stored consequently. They are located in the sensors list obtainable via `[#skinO_sensors](skin_object)`
	function, with indices in \[`skinM_sensor_ids_begin`, `[#skinM_sensor_ids_end]`). See also `[#skin_sensor_id](skin_object)`.

VARIABLE skinM_sensor_ids_end: skin_sensor_id
	Where the sensors belonging to this module end

	The sensors of each module are stored consequently. They are located in the sensors list obtainable via `[#skinO_sensors](skin_object)`
	function, with indices in \[`[#skinM_sensor_ids_begin]`, `skinM_sensor_ids_end`). See also `[#skin_sensor_id](skin_object)`.

FUNCTION skin_module: ()
	Class constructor

	Default constructor of the class. Sets the module variables so that the module would be invalid.

FUNCTION ~skin_module: ()
	Class destructor

	Destructor of the class.

FUNCTION skinM_sensors: (count: skin_sensor_size * = NULL): skin_sensor *
	Gives an array of sensors belonging to this module

	This function returns a reference to a part of the [sensors array](skin_object#skinO_sensors) where the sensors of this module are located.
	That is sensors from index `[#skinM_sensor_ids_begin]`. The array will have **`count`** elements. Note that this is **not** a copy of the sensors
	array, but a reference to the original. See also `[#skin_sensor_size](skin_object)`.

	INPUT count
		After `skinM_sensors` finishes, this variable will hold the number of sensors in this module. If `NULL` is passed, or the function is called with
		no inputs, this input will be ignored.
	OUTPUT
		As output, this function returns an array of sensors belonging to this module. If the module is not properly intialized, this function will
		return `NULL` and the input **`count`** will be left untouched.

FUNCTION skinM_patch: (): skin_patch *
	Gives the patch this module belongs to

	This functions outputs a reference to the patch this module belongs to. This is the same as writing
		```
		\&skin.[#skinO_patches](skin_object)(\&patches_count)\[module.[#skinM_patch_id]\]
		```
	where `skin` is the skin object (of type `[skin_object]`) and `module` is the `skin_module` object `skinM_patch` is being called for.

	OUTPUT
		A reference to the patch this module belongs to.

FUNCTION skinM_sensors_begin: (): skin_module::skin_sensor_iterator
	Gives an iterator to iterate over sensors of this module

	This function returns a pointer of type `skin_module::[skin_sensor_iterator]`, similar to C++'s STL `iterator` classes, which points to the first
	of this module's sensors. This iterator can then be used to traverse all sensors of this module. Note that the sensors traversed are not a copy
	and the iterators actually point to the original data.

	OUTPUT
		The output of this function is an iterator over sensors of this module, pointing to the first one.

FUNCTION skinM_sensor_end: (): const skin_module::skin_sensor_iterator
	Gives an invalid iterator marking the end of iteration over the sensors

	This function returns a pointer of type `skin_module::[skin_sensor_iterator]`, similar to C++'s STL `iterator` classes, which points to an invalid
	sensor. This iterator can then be used in comparison with another iterator to determine whether that iterator has reached the end of its range.
	This is similar to how iterators are used in C++'s STL.

	OUTPUT
		The output of this function is an iterator over sensors of this module pointing to an invalid sensor (and therefore should not be dereferenced).
