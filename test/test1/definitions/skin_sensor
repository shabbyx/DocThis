class skin_sensor
# The Skin Middleware
version version 0.8.0.486
author Shahbaz Youssefi
keyword skin
keyword middleware
keyword DIST
keyword MacLAB
shortcut index
shortcut globals
shortcut constants
previous class skin_object
next class skin_sensor_type
seealso `[skin_sensor_type]`
seealso `[skin_module]`
seealso `[skin_patch]`
seealso `[skin_sub_region]`
seealso `[skin_region_iterator]`
seealso `[#skinO_sensors](skin_object)`

The `skin_sensor` class holds data of one sensor in the skin.

VARIABLE skinS_id: skin_sensor_id
	Id of the sensor

	The id of the sensor. This value is equal to the sensor's index in the sensor list obtainable via `[#skinO_sensors](skin_object)` function.
	If the value of `skinS_id` is equal to `[#SKIN_INVALID_ID](constants)`, that means the sensor is not properly initialized. See also
	`[#skin_sensor_id](skin_object)`.

VARIABLE skinS_sub_region_id: skin_sub_region_id
	Id of the sub-region this sensor belongs to

	The id of the sub-region the sensor belongs to. See `[#skinSR_id](skin_sub_region)` and `[#skin_sub_region_id](skin_object)`.

VARIABLE skinS_module_id: skin_module_id
	Id of the module this sensor belongs to

	The id of the module the sensor belongs to. See `[#skinM_id](skin_module)` and `[#skin_module_id](skin_object)`.

VARIABLE skinS_sensor_type_id: skin_sensor_type_id
	Sensor type of the sensor

	The id of the sensor type the sensor belongs to. See `[#skinST_id](skin_sensor_type)` and `[#skin_sensor_type_id](skin_object)`

VARIABLE skinS_response: skin_sensor_response
	Response value of the sensor

	This variable holds the value of the response of the sensor. Note that it may not necessarily be up-to-date and
	therefore it is advised to get the value of the sensor response through `[#skinS_get_response]` function. See also `[#skin_sensor_response](skin_object)`.

VARIABLE skinS_relative_position: float[3]
	Relative position of the sensor on the robot link

	As explained under the short description of `skin_sensor` [here](index#skin_sensor), each sensor is assumed to be fixed
	with respect to the [robot link](#skinS_robot_link) it is connected to. Therefore, the value of `skinS_relative_position` determined
	through _[calibration](skin_object#skinO_calibration_end)_ or provided from [cache](skin_object#skinO_calibration_reload), would
	be the relative position of the sensor with respect to the robot link's reference frame. This value can later be used to
	compute the global position of the sensor given the current robot pose.

VARIABLE skinS_relative_orientation: float[3]
	Relative orientation of the sensor w.r.t the robot link

	Similar to `skinS_relative_position`, the value of `skinS_relative_orientation` determined through _[calibration](skin_object#skinO_calibration_end)_
	or provided from [cache](skin_object#skinO_calibration_reload), would be the relative orientation of the sensor
	with respect to the [robot link](#skinS_robot_link)'s reference frame. The value stored in this variable is the normal vector to the surface of
	the skin at this sensor. This value can later be used to compute the global orientation of the sensor given the current robot pose.

VARIABLE skinS_robot_link: uint32_t
	The robot link this sensor is located on

	This is an identifier for the robot link this sensor is located on. This is a value provided by the calibrator and is meaningless to the middle-ware.
	It can be used by the user application, together with `[#skinS_relative_position]` and `[#skinS_relative_orientation]` through the user's own
	kinematics library to update `[#skinS_global_position]` and `[#skinS_global_orientation]` respectively.

VARIABLE skinS_global_position: float[3]
	Global position of the sensor

	Global position of the sensor can be computed from `[#skinS_relative_position]` by the user program itself, and is not touched by the middle-ware.

VARIABLE skinS_global_orientation: float[3]
	Global orientation of the sensor

	Global orientation of the sensor can be computed from `[#skinS_relative_orientation]` by the user program itself, and is not touched by the middle-ware.

VARIABLE skinS_flattened_position: float[2]
	Position of the sensor in the 2D map of sensors

	Inspired by how the brain maps the 3D skin in 2D, there has been a 2D map incorporated in the library, and therefore
	each sensor would additionally have a 2D location indicating its location in the 2D map.

VARIABLE skinS_neighbors: skin_sensor_id *
	Neighbors of the sensor in the 2D map of sensors

	This array of ids, indicates which sensors are neighboring this sensor. These values are obtained as a result of
	loading the 2D map of the skin as described [here](#skinS_flattened_position). See also `[#skin_sensor_id](skin_object)`.

VARIABLE skinS_neighbors_count: skin_sensor_size
	Size of `skinS_neighbors`

	Holds the size of `[#skinS_neighbors]` array. See also `[#skin_sensor_size](skin_object)`.

FUNCTION skin_sensor: ()
	Class constructor

	Default constructor of the class. Sets the sensor variables so that the sensor would be invalid. `[#skinS_id]` will be equal to
	`[#SKIN_INVALID_ID](constants)` after this operation.

FUNCTION ~skin_sensor: ()
	Class destructor

	Destructor of the class.

FUNCTION skinS_get_response: (): skin_sensor_response
	Gives the response of the sensor

	Depending on the skin technology, the raw value of the response could be interpreted differently and therefore, the library is
	responsible for converting these values to a defined range (see `[#skinS_response]`) with 0 as the minimum possible value.
	The value computed will also be stored in `[#skinS_response]`. See also `[#skin_sensor_response](skin_object)`.

	OUTPUT
		The current up-to-date response of the sensor.

FUNCTION skinS_sensor_type: (): skin_sensor_type *
	Gives the sensor type this sensor belongs to

	This function outputs a reference to the sensor type this sensor belongs to. This is the same as writing
		```
		\&skin.[#skinO_sensor_types](skin_object)(\&sensor_types_count)\[sensor.[#skinS_sensor_type_id]\]
		```
	where `skin` is the skin object (of type `[skin_object]`) and `sensor` is the `skin_sensor` object `skinS_sensor_type` is being called for.

	OUTPUT
		A reference to the sub-region this sensor belongs to.

FUNCTION skinS_sub_region: (): skin_sub_region *
	Gives the sub-region this sensor belongs to

	This function outputs a reference to the sub-region this sensor belongs to. This is the same as writing
		```
		\&skin.[#skinO_sub_regions](skin_object)(\&sub_regions_count)\[sensor.[#skinS_sub_region_id]\]
		```
	where `skin` is the skin object (of type `[skin_object]`) and `sensor` is the `skin_sensor` object `skinS_sub_region` is being called for.

	OUTPUT
		A reference to the sub-region this sensor belongs to.

FUNCTION skinS_module: (): skin_module *
	Gives the module this sensor belongs to

	This functions outputs a reference to the module this sensor belongs to. This is the same as writing
		```
		\&skin.[#skinO_modules](skin_object)(\&modules_count)\[sensor.[#skinS_module_id]\]
		```
	where `skin` is the skin object (of type `[skin_object]`) and `sensor` is the `skin_sensor` object `skinS_module` is being called for.

	OUTPUT
		A reference to the module this sensor belongs to.

FUNCTION skinS_patch: (): skin_patch *
	Gives the patch this sensor belongs to

	This functions outputs a reference to the patch this sensor belongs to. This is the same as writing
		```
		\&skin.[#skinO_patches](skin_object)(\&patches_count)\[skin.[#skinO_modules](skin_object)(\&modules_count)\[sensor.[#skinS_module_id]\].[#skinM_patch_id](skin_module)\]
		```
	where `skin` is the skin object (of type `[skin_object]`) and `sensor` is the `skin_sensor` object `skinS_module` is being called for.

	OUTPUT
		A reference to the patch this sensor belongs to.

FUNCTION skinS_regions_begin: (): skin_sensor::skin_region_iterator
	Gives an iterator to iterate over regions containing this sensor

	This function returns a pointer of type `skin_sensor::[skin_region_iterator]`, similar to C++'s STL `iterator` classes, which points
	to the first of this sensor's regions. This iterator can then be used to traverse all regions containing this sensor. Note that the
	regions traversed are not a copy and the iterators actually point to the original data.

	OUTPUT
		The output of this function is an iterator over regions containing this sensor, pointing to the first one.

FUNCTION skinS_regions_end: (): const skin_sensor::skin_region_iterator
	Gives an invalid iterator marking the end of iteration over the regions

	This function returns a pointer of type `skin_sensor::[skin_region_iterator]`, similar to C++'s STL `iterator` classes, which points
	to an invalid region. This iterator can then be used in comparison with another iterator to determine whether that iterator has reached
	the end of its range. This is similar to how iterators are used in C++'s STL.

	OUTPUT
		The output of this function is an iterator over regions containing this sensor pointing to an invalid region (and therefore should not be dereferenced).
