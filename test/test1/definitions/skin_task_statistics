class skin_task_statistics
# The Skin Middleware
version version 0.8.0.486
author Shahbaz Youssefi
keyword skin
keyword middleware
keyword DIST
keyword MacLAB
shortcut index
shortcut globals
shortcut constants
previous class skin_communication
next class skin_object
seealso `[skin_communication]`

This simple class holds statistics for [data acquisition threads](skin_communication#skinC_start). From the
[communicator](skin_object#skinO_communicator), the `[#skinC_tasks_statistics](skin_communication)` array can be indexed with
layer id to retrieve these statistics for each layer individually. Remember that a layer refered to sensors of a
[sensor type](skin_sensor_type).

VARIABLE skinTS_worst_read_time: skinRT_time
	Worst case execution time of the reader thread

	This variable will hold the worst case executation time of the reader thread for this layer. The type
	`[#skinRT_time](skinRT)` is the same as RTAI-magma's `RTIME`.

VARIABLE skinTS_best_read_time: skinRT_time
	Best case execution time of the reader thread

	This variable will hold the best case executation time of the reader thread for this layer. The type
	`[#skinRT_time](skinRT)` is the same as RTAI-magma's `RTIME`.

VARIABLE skinTS_accumulated_read_time: skinRT_time
	Accumulated execution time of the reader thread

	This variable will hold the sum of executation time of each cycle of the reader thread for this layer since its beginning.
	The type `skinRT_time` is the same as RTAI-magma's `RTIME`. Divided by `[#skinTS_number_of_reads]`, an average execution time
	can be obtained. Note however that these values are updated with every read so in an unfortunate
	case, the two variables may not refer to the same timestamp.

VARIABLE skinTS_number_of_reads: uint32_t
	Number of frames read by the reader thread since its start

	This variable will hold the number of times the reader has read data since beginning of executation.
