/*
 * Copyright (C) 2011-2012 Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * This file is part of DocThis!.
 *
 * DocThis! is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DocThis! is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DocThis!.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SEMANTICS_H_BY_CYCLOPS
#define SEMANTICS_H_BY_CYCLOPS

#include <shcompiler.h>
#include <list>
#include <string>

using namespace std;

struct formattedText
{
	string html;
	string tex;
	string plain;		// This could be used for anchors and labels
	formattedText operator +(const formattedText &rhs)
	{
		formattedText res = *this;
		res.html += rhs.html;
		res.tex += rhs.tex;
		res.plain += rhs.plain;
		return res;
	}
	formattedText &operator +=(const formattedText &rhs)
	{
		html += rhs.html;
		tex += rhs.tex;
		plain += rhs.plain;
		return *this;
	}
};

struct docIdentifier
{
	formattedText name;
	string anchor;
};

enum docTypeEnum
{
	INVALID = 0,		// This file was not a valid DocThis! file
	INDEX,
	CLASS,
	STRUCT,
	FUNCTIONS,
	VARIABLES,
	API,
	CONSTANTS,
	IOFILE,
	EXAMPLE,
	OTHER,
	GLOBALS
};

struct docGlobalItem
{
	string filename;
	int line, column;	// line and column the filename was read
};

void stopEvaluation();
void resetSemantics();
void outputFormats(bool html, bool tex, bool summary, bool details);	// true or false for either to set which output would be generated
int readNotices(const string &noticeFile);
bool noError();
formattedText		docName();
formattedText		docTitle();
string			docAuthor();
docTypeEnum		docType();
string			docVersion();
list<string>		docKeywords();
list<docGlobalItem>	docGlobalsList();
void			docGlobals(list<docIdentifier> &macros, list<docIdentifier> &types, list<docIdentifier> &globalTypes,
				list<docIdentifier> &variables, list<docIdentifier> &globalVariables,
				list<docIdentifier> &functions, list<docIdentifier> &globalFunctions);
formattedText		docHeader();
formattedText		docBottom();

void docType(shParser *parser);
void setDocName(shParser *parser);
void fieldType(shParser *parser);
void setNextPrevType(shParser *parser);
void fieldValue(shParser *parser);
void seealsoValue(shParser *parser);
void pop(shParser *parser);
void push(shParser *parser);
void pushEmpty(shParser *parser);
void pushCodeWord(shParser *parser);
void pushLinkKeyword(shParser *parser);
void pushCode(shParser *parser);
void pushEmphasize(shParser *parser);
void pushStrong(shParser *parser);
void pushHTML(shParser *parser);
void pushAnchor(shParser *parser);
void pushNoLink(shParser *parser);
void pushYesLink(shParser *parser);
void pushInline(shParser *parser);
void pushIndent(shParser *parser);
void pushNewLine(shParser *parser);
void memberOrGlobal(shParser *parser);
void pushMacroDeclaration(shParser *parser);
void pushVariableDeclaration(shParser *parser);
void pushTypeDeclaration(shParser *parser);
void pushFunctionDeclaration(shParser *parser);
void pushInputDeclaration(shParser *parser);
void pushOutputDeclaration(shParser *parser);
void pushConstGroupDeclaration(shParser *parser);
void pushConstDeclaration(shParser *parser);
void pushImportance(shParser *parser);
void pushMightHaveSpace(shParser *parser);
void pushArg(shParser *parser);
void pushHasValue(shParser *parser);
void pushDefaultValue(shParser *parser);
void pushParagraphAndItems(shParser *parser);
void concat(shParser *parser);
void concatKeepsSpace(shParser *parser);
void concatIgnoresSpace(shParser *parser);
void concatStoreState(shParser *parser);
void concatRestoreState(shParser *parser);
void concatDefaultValue(shParser *parser);
void concatParagraphAndItems(shParser *parser);
void inLink(shParser *parser);
void notInLink(shParser *parser);
void closeItemsUntil(shParser *parser);
void headingBegin(shParser *parser);
void headingEnd(shParser *parser);
void itemBegin(shParser *parser);
void itemEnd(shParser *parser);
void paragraphEnd(shParser *parser);
void codeBlockBegin(shParser *parser);
void codeBlockEnd(shParser *parser);
void hrEnd(shParser *parser);
void importantCodeEnd(shParser *parser);
void definitionBegin(shParser *parser);
void definitionEnd(shParser *parser);
void makeLink(shParser *parser);
void readWhiteSpace(shParser *parser);
void ignoreWhiteSpace(shParser *parser);
void readNewLine(shParser *parser);
void ignoreNewLine(shParser *parser);
void setOverview(shParser *parser);
void setElementExplanation(shParser *parser);
void setNotice(shParser *parser);
void setShortExplanation(shParser *parser);
void checkCompleteness(shParser *parser);
void increaseIndent1(shParser *parser);
void decreaseIndent1(shParser *parser);
void increaseIndent2(shParser *parser);
void decreaseIndent2(shParser *parser);
void increaseIndent3(shParser *parser);
void decreaseIndent3(shParser *parser);
void increaseIndent4(shParser *parser);
void decreaseIndent4(shParser *parser);
void increaseIndent5(shParser *parser);
void decreaseIndent5(shParser *parser);
void addToGlobalList(shParser *parser);
void documentHeader(shParser *parser);
void documentBottom(shParser *parser);
void writeDocument(shParser *parser);

#endif
