## DocThis! Documentation Generator

This program converts definition files to HTML, LaTeX or man files. The format
of the definition files is function definitions with markdown descriptions.
libpandoc is used to convert the descriptions, so support for other description
formats is not difficult.

There are a few reasons why I wrote this program and don't use doxygen:

- With doxygen you mix code and documentation. This is terrible for a few
  reasons:

  * You are somewhat discouraged from writing complete (and thus long)
    documentations.  This is because your code gets cluttered with documentation
    and moving around the code becomes a pain.

  * As is seen in numerous projects, the documentation and the code are not
    matched, yet this remains undetected.  The reason for this is that, since
    the documentation is next to the code, it is assumed to be up-to-date by
    doxygen.  Since the programmer doesn't always remember to mark the comment
    above the function as "outdated", the programmer himself would also forget
    what needs to be updated.  Outdated documentation is very hard to detect
    when using doxygen.

- You can't read the documentation if you are in a system that doesn't have
  a browser or PDF viewer.

The good points about DocThis! are these:

- The html, tex and man file names are similar to the definition files and
  anchors are made based on the variable/type/function/etc name. So you easily
  create a link in the description of some element to any other part of the
  documentation.

- The documentation looks like what you want and contains what you tell it to.

- The output is customizable. There is a .css file (more themes to be added in
  the future) that can be used if the user so desires.

- It creates one output file per input file and thus, except the file containing
  list of globals, it doesn't need to regenerate the whole documentation on
  every build, but it would only do so for updated files (like compiling files
  from source files in a large project)

- The documentation and the code are apart (opposite of the negative point of
  doxygen as I already explained).  DocThis! requires you to provide the version
  number for the program/library as meta data of each page individually.  As the
  code progresses, versions change and outdated documentation pages are
  instantly identified.

- The source files of the documentation are good-looking documentations
  themselves!  So if you found yourself coding on a light linux that doesn't
  have a GUI, or you don't even have DocThis! to compile the documentation
  files, you can simply refer to the documentation source files. The syntax of
  DocThis! is designed specially to simplify links and make them understandable,
  so that the source file, while easy to write, is also perfectly readable.

- DocThis! requires you to have explanation, even if short, on everything. You
  cannot for example leave explanation of a function argument empty.  Often it
  happens that some arguments seems obvious to the programmer, leaving it
  undocumented only to cause confusion later for the user.  Not with DocThis!

The bad points are these:

- You need to write more than doxygen. One reason is that the things that are
  automatically done in doxygen, such as your function prototype need to be done
  manually here because DocThis! doesn't inspect your code.  This is however as
  small as identifier declarations and a small header.

I found the best way for documenting using DocThis! is this.  Look through your
ChangeLog, which could be your git history for example, and identify the changes
for which the documentation is not yet updated.  With git, this could be commits
from the last time you committed "Bring documentations up-to-date".  With a
ChangeLog file, this could be for example using `-` for new changes and `*` for
documented entries.  Then simply update those functions and bring the version
tag of the files to current.  The important point here is that if you forget
to update some files, or someone takes out-of-date documentation, it is
immediately obvious that the this is the case.

If you haven't updated the documentation for long, you can go through each
function one by one and check with source to make sure they still do what the
documentation says.  Once a file is updated and its version bumped, it will be
clear to both users and other developers what is up-to-date and what needs
updating.
