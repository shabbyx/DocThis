include Makefile.config
include Makefile.common

TARGET := docthis
RM := rm -f
BUILD_FILE := build.txt
SBUILD_FILE := sbuild.txt
INC_BUILD_PROG := ./increase_build
ifeq ($(OS_TYPE),Windows)
  INC_BUILD_LINK :=
  TARGET := $(TARGET).exe
  INC_BUILD_PROG := $(INC_BUILD_PROG).exe
else
  ifeq ($(OS_TYPE),Linux)
    INC_BUILD_LINK := -lrt
  else
    # Try with linux style, hopefully it will work
    INC_BUILD_LINK := -lrt
  endif
endif

.PHONY: all
all:
	@$(MAKE) --no-print-directory $(INC_BUILD_PROG)
	$(INC_BUILD_PROG) $(BUILD_FILE)
	@$(MAKE) --no-print-directory -C src objects
	@$(MAKE) --no-print-directory -C build
	@$(MAKE) --no-print-directory $(SBUILD_FILE)
#@$(MAKE) --no-print-directory -C doc
#@$(MAKE) --no-print-directory -C test
$(SBUILD_FILE): $(TARGET)
	$(INC_BUILD_PROG) $(SBUILD_FILE)
	@$(MAKE) --no-print-directory -C doc clean
	@$(MAKE) --no-print-directory -C test clean
$(INC_BUILD_PROG): increase_build.c
	gcc -o $@ $< $(INC_BUILD_LINK)
.PHONY: doc
doc:
	@$(MAKE) --no-print-directory -C doc
.PHONY: install uninstall
install uninstall:
	@$(MAKE) --no-print-directory -f Makefile.install $@
.PHONY: clean
clean:
	@$(MAKE) --no-print-directory -C src clean
	@$(MAKE) --no-print-directory -C build clean
	@$(MAKE) --no-print-directory -C test clean
	@$(MAKE) --no-print-directory -C doc clean
	-$(RM) $(INC_BUILD_PROG)
